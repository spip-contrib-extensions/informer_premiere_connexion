<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/informerpremiereconnexion?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'configuration_texte_label' => 'Message to display to the user who has confirmed their registration',
	'configuration_titre' => 'Configure the first connection message',

	// T
	'texte_defaut' => 'Your user account has been created!'
);
