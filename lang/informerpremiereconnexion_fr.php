<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/informer_premiere_connexion.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'configuration_texte_label' => 'Message à afficher à l’utilisateur⋅trice qui vient de confirmer son inscription',
	'configuration_titre' => 'Configurer le message de première connexion',

	// T
	'texte_defaut' => 'Votre compte utilisateur a bien été créé !'
);
