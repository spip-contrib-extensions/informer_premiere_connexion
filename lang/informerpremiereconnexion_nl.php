<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/informerpremiereconnexion?lang_cible=nl
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'configuration_texte_label' => 'Te tonen bericht bij een inschrijvingsbevestiging van een nieuwe gebruik(ste)er',
	'configuration_titre' => 'Het bericht bij een eerste verbinding configureren',

	// T
	'texte_defaut' => 'Je account is klaar voor gebruik!'
);
